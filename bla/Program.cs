﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 


namespace bla
{
    class bla
    {

        private static double[,] Tabela;
        private static double[,] TabelaAuxiliar;

        private int numVariaveis, numRestricoes;
        private static int linhaPermitida, colunaPermitida;
        private static double pivo, inversoPivo;

        private static byte MaxMin;// max = 0, min = -1

        public bla(int numVariaveis, int numRestricoes)
        {

            this.numVariaveis = numVariaveis;
            this.numRestricoes = numRestricoes;

            Tabela = new double[this.numVariaveis, this.numRestricoes];
            //Tabela = new double[2, 2] { { 1,2},{3,4} }; 

        }

        public static void mostrarTabela()
        {
            for (int i = 0; i < Tabela.GetLength(0); i++)
            {
                for (int j = 0; j < Tabela.GetLength(1); j++)
                {
                    Console.WriteLine(bla.Tabela[i, j]);
                }
            }
        }

        public static void iniciaTabela(int numVar, int numRest)
        {
            Tabela = new double[numRest+1, numVar+1];
 
        }

        public static void getFuncaoObjetivo()
        {
           for (int i = 1; i < Tabela.GetLength(1) ; i++)
            {
                Console.WriteLine("valor de x"+i);
                double num = Convert.ToInt32(Console.ReadLine());
                if (MaxMin == 0)
                {// quer dizer q é min
                    Tabela[0, i] = num*-1;
                }
                else
                {
                    Tabela[0, i] = num;
                }
                
            }
        }

        public static void getResricoes()
        {
            for (int i = 1; i < Tabela.GetLength(0); i++)
            {
                Console.WriteLine("Restricao " +i);
                for (int j = 0; j < Tabela.GetLength(1); j++)
                {
                    Console.WriteLine("valor de x" + j);
                    double num = Convert.ToInt32(Console.ReadLine());
                    Tabela[i, j] = num;
                }
            }
        }

        public static void mostrarRestricoes()
        {
            for (int i = 1; i < Tabela.GetLength(1); i++)
            {
                Console.WriteLine("Restricao " + i);

                for (int j = 0; j < Tabela.GetLength(1); j++)
                {
                    Console.WriteLine("valor de x" + j + " = " + Tabela[i, j]);
                }
            }
        }

        public static void mostrarFO()
        {
            for (int i = 0; i < Tabela.GetLength(1); i++)
            {
                Console.WriteLine("valor de x" + i +" = "+ Tabela[0, i]);
            }
        }

        public void getRestricoes(int numRest)
        {

        }

        //====================< Metodo Simplex >=======================

            // procura uma variável Basica cujo membro livre é negativo
        public static int getVarBasicaMLNegativo()
        { 
            int linha = -1;
            bool continuaProcurando = true;
            for (int i = 1; (i< Tabela.GetLength(0) && continuaProcurando); i++)// começo do 1 pq a linha 0 corresponde a linha do fx
            {
                if (Tabela[i, 0]<0)
                {
                    linha = i;
                    Console.WriteLine(" a linha escolhina foi: " + i + " o numero foi: " + Tabela[i, 0]);

                    for (int j = 1; j < Tabela.GetLength(1); j++)// começo da 1 coluna pq a coluna 0 corresponde ao membro livre
                    {
                        if (Tabela[i, j] < 0)
                        {
                            colunaPermitida = j;
                            Console.WriteLine(" a coluna escolhina foi: " + j + " o numero foi: " + Tabela[i, j]);
                            continuaProcurando = false;
                            break;
                        }
                    }
                }
            }
            //getColunaPermitida(linha);
            return linha;
        }
        public static void getLinhaPermitida()
        {
            double teste = 999999999;

            for (int i = 1; (i < Tabela.GetLength(0)); i++)// começo do 1 pq a linha 0 corresponde a linha do fx
            {
                
                // confiro apenas se tiverem o mesmo sinal e se o valor da coluna permitida for diferente de 0
                if (((Tabela[i, colunaPermitida] > 0 && Tabela[i, 0] > 0) || (Tabela[i, colunaPermitida] < 0 && Tabela[i, 0] < 0)) && (Tabela[i, colunaPermitida] != 0)) {
                    if ((Tabela[i, 0] / Tabela[i, colunaPermitida]) < teste)
                    {
                        teste = (Tabela[i, 0] / Tabela[i, colunaPermitida]);
                        pivo = Tabela[i, colunaPermitida];
                        inversoPivo = 1/ Tabela[i, colunaPermitida];
                        TabelaAuxiliar[i, colunaPermitida] = inversoPivo;
                        linhaPermitida = i;
                    }
                }
            }
        }

        public static void atualizaPatrtedeBaixoTabela()
        {
            mostrTabelaAuxiliar();

            for (int i = 0; (i < Tabela.GetLength(0)); i++)// começo do 1 pq a linha 0 corresponde a linha do fx
            {
                if (i != linhaPermitida)
                {
                    TabelaAuxiliar[i, colunaPermitida] = Tabela[i, colunaPermitida] * (-1 * inversoPivo);
                }
            }
            //mostrTabelaAuxiliar();
            for (int i = 0; (i < Tabela.GetLength(1)); i++)// começo do 1 pq a linha 0 corresponde a linha do fx
            {
                if (i != colunaPermitida)
                {
                    TabelaAuxiliar[linhaPermitida, i] = Tabela[linhaPermitida, i]  * inversoPivo;
                }
            }
            //mostrTabelaAuxiliar();
            //Console.WriteLine("");
            for (int i = 0; (i < Tabela.GetLength(0)); i++)
            {
                for (int j = 0; (j < Tabela.GetLength(1)); j++)
                {
                    if (i != linhaPermitida && j != colunaPermitida)
                    {
                       // Console.WriteLine("linha ["+ i +"]: "+ TabelaAuxiliar[i, colunaPermitida] + " * coluna [" + j + "]" + Tabela[linhaPermitida, j]);
                        TabelaAuxiliar[i, j] = TabelaAuxiliar[i, colunaPermitida] * Tabela[linhaPermitida, j];
                    }
                }
            }

        }

        public static void mostrTabelaAuxiliar()
        {
            Console.Write("\n");
            for (int i = 0; (i < Tabela.GetLength(0)); i++)
            {
                for (int j = 0; (j < Tabela.GetLength(1)); j++)
                {

                    Console.Write(TabelaAuxiliar[i, j]+ " ");
                }
                Console.Write("\n");
            }
        }

        public static void mostrTabela()
        {
            Console.Write("\n");
            for (int i = 0; (i < Tabela.GetLength(0)); i++)
            {
                for (int j = 0; (j < Tabela.GetLength(1)); j++)
                {

                    Console.Write(Tabela[i, j] + " ");
                }
                Console.Write("\n");
            }
        }

        public static void getColunaPermitida(int linha)
        {
            for (int i = linha; i < Tabela.GetLength(1); i++)// começo da 1 coluna pq a coluna 0 corresponde ao membro livre
            {
                if (Tabela[linha, i] < 0)
                {
                    colunaPermitida = i;
                    Console.WriteLine(" a coluna escolhina foi: " + i + " o numero foi: " + Tabela[linha, i]);
                    break;
                }

            }
        }

        public static bool isImpossivel()
        {
            bool impossivel = true;

            for (int i = 1; i < Tabela.GetLength(0); i++)// começo da 1 coluna pq a coluna 0 corresponde ao membro livre
            {
                if (Tabela[i, 0] < 0)
                {
                    //Console.WriteLine(" linha: " + i + " valor: " + Tabela[i, 0]);
                    for (int j = 1; j < Tabela.GetLength(1); j++)// começo da 1 coluna pq a coluna 0 corresponde ao membro livre
                    {
                        if (Tabela[i, j] < 0)
                        {
                            //Console.WriteLine(" coluna: " +j + " valor: " + Tabela[i, j]);
                            impossivel = false;
                        }
                    }           
                }
            }

            return impossivel;

        }

        public static bool isIlimitado()
        {
            bool ilimitado = false;
            for (var i = 1; i < Tabela.GetLength(1); i++)// começo da 1 coluna pq a coluna 0 corresponde ao membro livre
            {
                if (Tabela[0, i] > 0)
                {
                    for(var j = 1; j < Tabela.GetLength(0); j++)
                    {
                        if (Tabela[j, i] > 0)
                            return false;
                        else ilimitado = true;
                    }
                }
                
            }

            return ilimitado;

        }

        public static bool isIndeterminado()
        {
            bool indeterminado = true;
            bool existeZero = false;

            for (int i = 1; i < Tabela.GetLength(1); i++)// começo da 1 coluna pq a coluna 0 corresponde ao membro livre
            {
                if ((Tabela[0, i] > 0))
                {
                    indeterminado = false;
                }
                else if (Tabela[0, i] == 0)
                {
                    existeZero = true;
                }
            }
            // posso ter todos 0? tendo todos 0 é indeterminado?
            if(existeZero && indeterminado)
            {
                indeterminado = true;
            }

            return indeterminado;

        }
        public static bool isPossivel()
        {
            bool possivel = true;
            for (int i = 1; i < Tabela.GetLength(1); i++)
            {
                Console.WriteLine("valor: "+ Tabela[0, i] +" > 0 ?");
                if (Tabela[0, i] > 0)
                {
                    Console.WriteLine(i);
                    possivel = false;

                }
            }
            return possivel;
        }

        public static void algoritmoTroca()
        {
            for (int i = 0; i < Tabela.GetLength(0); i++)
            {
                for (int j = 0; j < Tabela.GetLength(1); j++)
                {
                    if (i == linhaPermitida || j == colunaPermitida)
                    {
                        Tabela[i, j] = TabelaAuxiliar[i, j];
                    }
                    else{
                        Tabela[i, j] = Tabela[i, j] + TabelaAuxiliar[i, j];
                    }
                }
            }

        }



        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            
            // Console.WriteLine("Quantas variaveis?");
            //  String s = Console.ReadLine();

            //  Console.WriteLine("Quantas restricoes?");
            //  String b = Console.ReadLine();

            //    Console.WriteLine("É Mim(0) ou Max(1)??");
            //   bla.MaxMin = Convert.ToByte(Console.ReadLine());

            //  iniciaTabela(Convert.ToInt32(s), Convert.ToInt32(b));

            // Console.WriteLine("linha= "+Tabela.GetLength(0)+" colunas= "+Tabela.GetLength(1));

            Tabela = new double[4, 3] { { 0,    2,  3 }, 
                                        { -16,  -3,  -2 }, 
                                        { 6,    -2,  -1 }, 
                                        { -28,  -2,   -7 } };

            TabelaAuxiliar = new double[Tabela.GetLength(0), Tabela.GetLength(1)];

            for (int i = 0; (i < Tabela.GetLength(0)); i++)
            {
                for (int j = 0; (j < Tabela.GetLength(1)); j++)
                {
                    
                        TabelaAuxiliar[i, j] = -1;
                    
                }
            }

            //mostrTabelaAuxiliar();


            Console.WriteLine($"Impossivel? {isImpossivel()}");
            Console.WriteLine($"Ilimitado? {isIlimitado()}");
            Console.WriteLine($"Indeterminado? {isIndeterminado()}");
            Console.WriteLine("possivel? "+isPossivel());


            getVarBasicaMLNegativo();
            getLinhaPermitida();

            atualizaPatrtedeBaixoTabela();

            mostrTabela();
            mostrTabelaAuxiliar();

            algoritmoTroca();
            mostrTabela();



            //getFuncaoObjetivo();
            // getResricoes();
            // mostrarFO();


            //   bla.getVariaveis(Convert.ToInt32(s), Convert.ToInt32(b));
            //    mostrarTabela();

            Console.ReadKey();
        }
    }
}
